package com.airbnb.tests;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import libs.CommonUsedWebElements;
import libs.ConfigData;
import libs.ExcelDriver;
import libs.Util;

import org.apache.log4j.Logger;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

import pages.AirBnB;

public class Test3a {
	private WebDriver driver;
	AirBnB airBnB;
	static Logger log;
	static String appUrl;
	String appTitle;
	static Map<String, String> data = new HashMap<String, String>();
	boolean isTestPassed = true;
	// private TestData testData;
	Util util;

	
	/*
	 * This method is used to Set Up all our ativities e.g. constructors, waits
	 * etc.
	 */

	@Before
	public void setUp() throws Exception {
		util = new Util(driver);
		Util.killAllProcesses("firefox");
		driver = new FirefoxDriver();
		airBnB = new AirBnB(driver);
		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		log = Logger.getLogger(CommonUsedWebElements.class.getName());
		// testData = new TestData();
		appUrl = ConfigData.getCfgValue("APPLICATION_URL");
		appTitle = "airbnb";
		data = ExcelDriver.getData("src/testData.xls", "loginPage");
		log.info("Set Up finished succesful");
	}

	
	/*
	 * This test is used to check user login, edit personal page and log out
	 */

	@Test
	public void testEditPersonalPage() throws ClassNotFoundException, InstantiationException, IllegalAccessException, IOException, InterruptedException {
		log.info("Test1 is started");
		
//		airBnB.homePageByAnnotation.
		
		util.clearCookies();
		util.clearCash();
//		airBnB.homePageByDriver.verifyHomePage("HomePage.welcomeHeader", "HomePage.howItWorksButton");

		log.info("Checking that we are not logged in");
		Assert.assertEquals(false, airBnB.web.isloggedIn("EveryPage.envelopIcon", "EveryPage.userProfileImage"));
		log.info("We are logged in, ready to enter user credentioals");
		
//		airBnB.homePageByDriver.clickOnLogInLink();
		log.info("User credentials: login - " + data.get("login") + ", password - " + data.get("password"));

//		airBnB.loginPagebyDriver.loginUser(data.get("login"), data.get("password"), data.get("checkboxState"));
		
//		airBnB.homePageByDriver.verifyHomePage("HomePage.welcomeHeader", "HomePage.howItWorksButton");

		log.info("We are entered user credentials and pressed log in button, starting verifying that user is logged in");
		airBnB.web.isloggedIn("EveryPage.envelopIcon", "EveryPage.userProfileImage");
//		airBnB.homePageByDriver.userLogOut();
	}
	
	@After
	public void tearDown() throws Exception {
		log.info("Quit the driver");
		airBnB.web.quit();
	}

}






