package com.airbnb.tests;

import static org.junit.Assert.fail;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import libs.CommonUsedWebElements;
import libs.ExcelDriver;
import libs.ConfigData;
import libs.Util;

import org.apache.log4j.Logger;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

import pages.AirBnB;

import static libs.Util.testResult;

public class Test2b {
	private WebDriver driver;
	AirBnB airBnB;
	static Logger log;
//	private TestData testData;
	static String appUrl;
	String appTitle;
	//static String dataFile;
	static Map<String, String> data = new HashMap<String, String>();
	boolean isTestPassed = true;
	Util util;

	/*
	 * This method is used to Set Up all our ativities e.g. constructors, waits
	 * etc.
	 */

	@Before
	public void setUp() throws Exception {
		util = new Util(driver);	
		Util.killAllProcesses("firefox");		
		driver = new FirefoxDriver();
		airBnB = new AirBnB(driver);		
		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);		
		log = Logger.getLogger(CommonUsedWebElements.class.getName());
		
//		testData = new TestData();
		
		appUrl = ConfigData.getCfgValue("APPLICATION_URL");
		//dataFile = ConfigData.getCfgValue("DATA_FILE");
		appTitle = "airbnb";
		data = ExcelDriver.getData("src/testData.xls", "loginPage");		
			
	}

	/*
	 * This test is used to tet user log in function
	 */

	@Test
	public void test1() throws Exception {
		
		log.info("Test1 is started");
		
		airBnB.web.open(appUrl, appTitle);
		
		util.clearCookies();
		util.clearCash();
		
		airBnB.homePageByDriver.verifyHomePage("HomePage.welcomeHeader",
				"HomePage.howItWorksButton");

		log.info("Chacking that we are not logged in");
		Assert.assertEquals(false, airBnB.web.isloggedIn(
				"EveryPage.envelopIcon", "EveryPage.userProfileImage"));
		
		log.info("We are logged in, ready to enter user credentioals");
		airBnB.homePageByDriver.clickOnLogInLink();
		log.info("User credentials: login - " + data.get("login") + ", password - " + data.get("password"));

		airBnB.loginPagebyDriver.loginUser(data.get("login"), data.get("password"), data.get("checkboxState"));
		
		airBnB.homePageByDriver.verifyHomePage("HomePage.welcomeHeader",
				"HomePage.howItWorksButton");

		log.info("We are entered user credentials and pressed log in button, starting verifying that user is logged in");
		
		airBnB.web.isloggedIn("EveryPage.envelopIcon", "EveryPage.userProfileImage");
		
		isTestPassed = isTestPassed & airBnB.web.isloggedIn("EveryPage.envelopIcon", "EveryPage.userProfileImage");
		testResult(isTestPassed);
		
		log.info("User is logged in, Test1 is finished, ready to quit");
	}

	@After
	public void tearDown() throws Exception {
		log.info("Quit the driver");
		airBnB.web.quit();
	}
}
