package com.airbnb.tests;

/*
 * This class is used to collect and manipulate testData
 */

public class TestData {
	private String email = "vladimir.uroshlev@gmail.com";
	private String password = "10109595order";
	private String checkboxState = "YES";
	private String firstName = "Volodymyr";
	private String lastName = "Uroshliev";
	private String paswordConfirm = "10109595order";
	
	public TestData() {
		
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getCheckboxState() {
		return checkboxState;
	}

	public void setCheckboxState(String checkboxState) {
		this.checkboxState = checkboxState;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getPaswordConfirm() {
		return paswordConfirm;
	}

	public void setPaswordConfirm(String paswordConfirm) {
		this.paswordConfirm = paswordConfirm;
	}
		
}
