package com.airbnb.tests;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import libs.CommonUsedWebElements;

import org.apache.log4j.Logger;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

import pages.AirBnB;

public class Test1b {
	private WebDriver driver;
	AirBnB airBnB;
	static Logger log;
	private TestData testData;

	@Before
	public void setUp() throws Exception {
		driver = new FirefoxDriver();
		log = Logger.getLogger(Test1b.class.getName());
		airBnB = new AirBnB(driver);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		testData = new TestData();
		testData.setEmail("order12a@yandex.ru");
//		testData.setEmail("test@ya.ru");
	}
	
	/*
	 * This test is used register new user
	 */
	
	@Test
	public void testRegistration() throws ClassNotFoundException, InstantiationException, IllegalAccessException, IOException, InterruptedException {
		airBnB.web.open("http://en.airbnb.com", "airbnb");
		log.info("AirBnB page is opened");
		airBnB.homePageByDriver.verifyHomePage("HomePage.welcomeHeader", "HomePage.howItWorksButton");
		airBnB.homePageByDriver.clickSignUpLink("HomePage.signUpLink");
		airBnB.homePageByDriver.signUpByEmail("HomePage.signUpByEmailLink");
		if (airBnB.web.isloggedIn("EveryPage.envelopIcon", "EveryPage.userProfileImage")) {
//			airBnB.web.logout(); 
			log.info("We are already logged in, log out please");
		} 
		
		airBnB.registrationPageByDriver.registerNewUser(testData.getFirstName(), testData.getLastName(), testData.getEmail(),
				testData.getPassword(), testData.getPaswordConfirm(), testData.getCheckboxState());
		
		log.info("New user was created");
	}
	
	@After
	  public void tearDown() throws Exception {
		log.info("Quit our test");
	    airBnB.web.quit();
	  }
}
