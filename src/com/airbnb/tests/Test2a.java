package com.airbnb.tests;

import java.util.concurrent.TimeUnit;

import libs.CommonUsedWebElements;

import org.apache.log4j.Logger;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

import pages.AirBnB;

public class Test2a {
	private WebDriver driver;
	  AirBnB airBnB;
	  static Logger log;
	  private TestData testData;

	  /*
	   * This method is used to Set Up all our ativities
	   * e.g. constructors, waits etc.
	   */
	  
	  @Before
	  public void setUp() throws Exception {		  
		  driver = new FirefoxDriver();
		  airBnB = new AirBnB(driver);
		  driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		  log = Logger.getLogger(CommonUsedWebElements.class.getName());
		  testData = new TestData();
		  log.info("Set Up finished succesful");
	  }

	  /*
	   *This test is used to user log in 
	   */
	  
	  @Test
	  public void testLogInByAnnotation() throws Exception {
		  log.info("Starting our Test");
		  airBnB.web.open("http://en.airbnb.com", "airbnb");
		  airBnB.homePageByDriver.verifyHomePage("HomePage.welcomeHeader", "HomePage.howItWorksButton");
		  
		  log.info("Checking that we are not logged in");
		  
		  if(!airBnB.web.isloggedIn("EveryPage.envelopIcon", "EveryPage.userProfileImage")){
			  log.info("We are not logged in. So far, so good.");
		  }else{
			  log.info("We are logged in, need log out");
		  }
		  log.info("We are logged in, ready to enter user credentioals");
		  
		  airBnB.homePageByDriver.clickOnLogInLink();
		  airBnB.loginPageByAnnotation.userLogIn(testData.getEmail(), testData.getPassword(), testData.getCheckboxState());
		  airBnB.homePageByDriver.verifyHomePage("HomePage.welcomeHeader", "HomePage.howItWorksButton");
		  
		  log.info("We are entered user credentials and pressed log in button, starting verifying that user is logged in");
		  
		  if(airBnB.web.isloggedIn("EveryPage.envelopIcon", "EveryPage.userProfileImage")){
			 log.info("Log In was succesful"); 
		  }else{
			 log.info("We are no logged in"); 
		  }
		  
		  log.info("User is logged in, Test1 is finished, ready to quit");
	  }

	  @After
	  public void tearDown() throws Exception {
		log.info("Finishing our test");
	    airBnB.web.quit();
	  }
}
