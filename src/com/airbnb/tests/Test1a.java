package com.airbnb.tests;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import libs.CommonUsedWebElements;

import org.apache.log4j.Logger;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

import pages.AirBnB;

public class Test1a {
	private WebDriver driver;
	  AirBnB airBnB;
	  static Logger log;
	  private TestData testData;
	  
	  @Before
	  public void setUp() throws IOException{
		  driver = new FirefoxDriver();
		  airBnB = new AirBnB(driver);
		  driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		  log = Logger.getLogger(CommonUsedWebElements.class.getName());
		  testData = new TestData();
		  testData.setEmail("bla-bla-blaOne@mail.ru");
		  log.info("Set Up finished succesful");
	  }
	  
	  @Test
	  public void testRegistrationByAnnotation() throws ClassNotFoundException, InstantiationException, IllegalAccessException, IOException, InterruptedException{
		  log.info("Starting our Test");
		  airBnB.web.open("http://en.airbnb.com", "airbnb");
		  airBnB.homePageByDriver.verifyHomePage("HomePage.welcomeHeader", "HomePage.howItWorksButton");
		  
		  log.info("Checking that we are not logged in");
		  
		  if(!airBnB.web.isloggedIn("EveryPage.envelopIcon", "EveryPage.userProfileImage")){
			  log.info("We are not logged in. So far, so good.");
		  }else{
			  log.info("We are logged in, need log out");
		  }
		  
		  airBnB.homePageByDriver.clickSignUpLink("HomePage.signUpLink");
		  airBnB.homePageByDriver.signUpByEmail("HomePage.signUpByEmailLink");
		  
		  airBnB.registrationPageByAnnotation.registerNewUser(testData.getFirstName(), testData.getLastName(), 
				  testData.getEmail(), testData.getPassword(), testData.getPaswordConfirm(), testData.getCheckboxState());
		  log.info("New user was created");
	  }
	  
	  @After
	  public void tearDown(){
		  log.info("Finishing our test");
		  airBnB.web.quit();
	  }
}
