package com.airbnb.tests;

import static org.junit.Assert.fail;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import libs.CommonUsedWebElements;
import libs.ExcelDriver;
import libs.ConfigData;
import libs.Util;

import org.apache.log4j.Logger;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

import pages.AirBnB;

import static libs.Util.testResult;

public class Test3b {
	private WebDriver driver;
	AirBnB airBnB;
	static Logger log;
	private TestData testData;
	String appUrl;
	static Map<String, String> data = new HashMap<String, String>();
	boolean isTestPassed = true;
	Util util;

	/*
	 * This method is used to Set Up all our ativities e.g. constructors, waits
	 * etc.
	 */

	@Before
	public void setUp() throws Exception {
		Util.killAllProcesses("firefox");
		
		driver = new FirefoxDriver();
		airBnB = new AirBnB(driver);
		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		log = Logger.getLogger(CommonUsedWebElements.class.getName());
		testData = new TestData();
//		appUrl = ConfigData.getCfgValue("APPLICATION_URL");
//		data = ExcelDriver.getData("src/testData.xls", "LoginIntoApp");
		util = new Util(driver);	
		
	}

	/*
	 * This test is used to tet user log in function, navigating to dashboard and edit profile
	 */

	@Test
	public void testEditPersonalPage() throws Exception {
		
		log.info("Test1 is started");
		
		airBnB.homePageByDriver.verifyHomePageOpened("http://en.airbnb.com", "airbnb");
		
		util.clearCookies();
		util.clearCash();
		
		airBnB.homePageByDriver.verifyHomePage("HomePage.welcomeHeader",
				"HomePage.howItWorksButton");

		log.info("Checking that we are not logged in");
		Assert.assertEquals(false, airBnB.web.isloggedIn(
				"EveryPage.envelopIcon", "EveryPage.userProfileImage"));
		log.info("We are logged in, ready to enter user credentioals");
		
		airBnB.homePageByDriver.clickOnLogInLink();

		airBnB.loginPagebyDriver.loginUser(testData.getEmail(), testData.getPassword(), testData.getCheckboxState());
		
		airBnB.homePageByDriver.verifyHomePage("HomePage.welcomeHeader",
				"HomePage.howItWorksButton");

		log.info("We are entered user credentials and pressed log in button, starting verifying that user is logged in");
		
		airBnB.web.selectMenuElementByFocus("Dashboard", "EveryPage.userProfileName");		
		airBnB.dashboardPageByDriver.clickEditProfile();		
		airBnB.editProfilePageByDriver.enterTextIntoDescribeYourselfTextarea();
		airBnB.editProfilePageByDriver.saveEditedPersonalData();
		String textToVerify = airBnB.editProfilePageByDriver.getGeneratedText();		
		airBnB.viewUserProfilePageByDriver.checkText(textToVerify);		
		airBnB.homePageByDriver.focusOnHelp();
		airBnB.homePageByDriver.returnToHomePage();
	
		isTestPassed = isTestPassed & airBnB.web.isloggedIn("EveryPage.envelopIcon", "EveryPage.userProfileImage");
		testResult(isTestPassed);
		log.info("Test3b is finished, ready to quit");
		airBnB.homePageByDriver.userLogOut();
	}

	@After
	public void tearDown() throws Exception {
		log.info("Quit the driver");
		airBnB.web.quit();
	}
}







