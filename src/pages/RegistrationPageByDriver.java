package pages;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import libs.CommonUsedWebElements;
import static libs.ConfigData.ui;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class RegistrationPageByDriver {
	static  WebDriver driver;
	CommonUsedWebElements webElements;
	WebDriverWait explicitWait;
	
	/*
	 * At this page we register new user
	 */

	//Constructor for this class
	
	public RegistrationPageByDriver(WebDriver driver) throws IOException {
		this.driver = driver;
		webElements = new CommonUsedWebElements(driver);
		
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);//Implicit Waits
		
		explicitWait = new WebDriverWait(driver, 10);
	}
	
	/*
	 * In this method we enter First name
	 */
	
	public void enterFirstName(String firstname) throws IOException, ClassNotFoundException, IllegalAccessException, InstantiationException {
//		WebElement firstName;
//		firstName = driver.findElement(By.xpath(".//input[@name='user[first_name]']"));
//		firstName.clear();
//		firstName.sendKeys(firstname);
		explicitWait.until(ExpectedConditions.presenceOfElementLocated(ui("RegistrationPage.firstNameField")));
		webElements.inputText("Vladimir", "RegistrationPage.firstNameField");
	}
		
	
	/*
	 * In this method we enter Last name
	 */
	
	public void enterLastName(String lastname) throws IOException, ClassNotFoundException, IllegalAccessException, InstantiationException {
//		WebElement lastName;
//		lastName = driver.findElement(By.xpath(".//input[contains (@placeholder, 'Last name')]"));
//		lastName.clear();
//		lastName.sendKeys(lastname);
		
		webElements.inputText("Uroshlev", "RegistrationPage.lastNameField");
	}
	
		
	/*
	 * In this method we enter email address
	 */
	
	public void inputEmailAddress(String email) throws IOException, ClassNotFoundException, IllegalAccessException, InstantiationException {
//		WebElement emailAddress;
//		emailAddress = driver.findElement(By.xpath(".//input[@name='user[email]']"));	
//		emailAddress.clear();
//		emailAddress.sendKeys(email);
		
		webElements.inputText(email, "RegistrationPage.emailAddressField");
	}
		
	
	/*
	 * In this method we enter password that we want to use
	 * when creating account
	 */
	
	public void inputPassword(String password) throws IOException, ClassNotFoundException, IllegalAccessException, InstantiationException {
//		WebElement passwordField;
//		passwordField = driver.findElement(By.xpath(".//input[@name='user[password]']"));
//		passwordField.clear();
//		passwordField.sendKeys(password);
		
		webElements.inputText(password, "RegistrationPage.setPasswordField");
	}
		
	
	/*
	 * This method is used to confirm our password
	 */
	
	public void inputConfirmPassword(String passwordConfirm) throws IOException, ClassNotFoundException, IllegalAccessException, InstantiationException {
//		WebElement confirmPassword;
//		confirmPassword = driver.findElement(By.xpath(".//input[@name='user[password_confirmation]']"));
//		confirmPassword.clear();
//		confirmPassword.sendKeys(passwordConfirm);
		
		webElements.inputText(passwordConfirm, "RegistrationPage.confirmPasswordField");
	}
		
	
	/*
	 * This method is used to deactivate or activate our checkbox
	 */
	
	public void setNewsCheckboxState(String checkboxState) throws IOException, ClassNotFoundException, IllegalAccessException, InstantiationException {
//		WebElement newsCheckbox;
//		newsCheckbox = driver.findElement(By.xpath(".//input[@type='checkbox']"));
//		newsCheckbox.click();		
//		webElements.uncheckCheckedCheckBox(".//label[@class='checkbox']/input[@type='checkbox']");
		
		webElements.selectCheckBox(checkboxState, "RegistrationPage.setChaeckboxField");
	}
		
	
	/*
	 * This method is used to press Sign Up button 
	 * and register new user
	 */
	
	public void enterSignUp() throws IOException, ClassNotFoundException, IllegalAccessException, InstantiationException {
//		WebElement signUp;
//		signUp = driver.findElement(By.xpath(".//button[@type='submit']"));
//		signUp.click();
		
//		explicitWait.until(ExpectedConditions.elementToBeClickable(ui("RegistrationPage.signUpButton")));
		webElements.clickButton("RegistrationPage.signUpButton");
		
	}
	
	/*
	 * This method is used to register new user
	 */
	
	public void registerNewUser(String firstName, String LastName, String email, String password, String passwordConfirm, String checkboxState) throws IOException, ClassNotFoundException, IllegalAccessException, InstantiationException {
//		explicitWait.until(ExpectedConditions.titleIs("Vacation Rentals, Homes, Apartments & Rooms for Rent - Airbnb"));
		enterFirstName(firstName);
		enterLastName(LastName);
		inputEmailAddress(email);
		inputPassword(password);
		inputConfirmPassword(passwordConfirm);
		setNewsCheckboxState(checkboxState);
		enterSignUp();
	}
}
