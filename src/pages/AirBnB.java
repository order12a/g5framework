package pages;

import java.io.IOException;

import libs.CommonUsedWebElements;

import org.apache.log4j.Logger;
import org.openqa.selenium.WebDriver;

public class AirBnB {
	public HomePageByAnnotation homePageByAnnotation;
	public HomePageByDriver homePageByDriver;
	
	public LogInPageByDriver loginPagebyDriver;
	public LogInPageByAnnotation loginPageByAnnotation;
	
	public RegistrationPageByDriver registrationPageByDriver;
	public RegistrationPageByAnnotation registrationPageByAnnotation;
	
	public DashboardPageByAnnotation dashboardPageByAnnotation;
	public DashboardPageByDriver dashboardPageByDriver;
	
	public EditProfilePageByAnnotation editProfilePageByAnnotation;
	public EditProfilePageByDriver editProfilePageByDriver;
	
	public ViewUserProfilePageByDriver viewUserProfilePageByDriver;
	public ViewUserProfilePageByAnnotation viewUserProfilePageByAnnotation;
	
	public InfoPageByAnnotation infoPageByAnnotation;
	public InfoPageByDriver infoPageByDriver;
	
	public SearchResultPageByAnnotation searchResultPageByAnnotation;
	public SearchResultPageByDriver searchResultPageByDriver;
	
	public CommonUsedWebElements web;
	
	static Logger log = Logger.getLogger(AirBnB.class.getName());
	
	/*
	 * This class is used to initialize all pages of testing App
	 */

	public AirBnB(WebDriver driver) throws IOException {
		homePageByAnnotation = new HomePageByAnnotation(driver);
		homePageByDriver = new HomePageByDriver(driver);
				
		loginPagebyDriver = new LogInPageByDriver(driver);
		loginPageByAnnotation = new LogInPageByAnnotation(driver);
		
		registrationPageByDriver = new RegistrationPageByDriver(driver);
		registrationPageByAnnotation = new RegistrationPageByAnnotation(driver);
		
		dashboardPageByAnnotation = new DashboardPageByAnnotation(driver);
		dashboardPageByDriver = new DashboardPageByDriver(driver);
		
		editProfilePageByAnnotation = new EditProfilePageByAnnotation(driver);
		editProfilePageByDriver = new EditProfilePageByDriver(driver);
		
		viewUserProfilePageByAnnotation = new ViewUserProfilePageByAnnotation(driver);
		viewUserProfilePageByDriver = new ViewUserProfilePageByDriver(driver);
		
		infoPageByAnnotation = new InfoPageByAnnotation(driver);
		infoPageByDriver = new InfoPageByDriver(driver);
		
		searchResultPageByAnnotation = new SearchResultPageByAnnotation(driver);
		searchResultPageByDriver = new SearchResultPageByDriver(driver);
		
		web = new CommonUsedWebElements(driver);
		log.info("AirBnB page is initialized");
	}
	
	
}
