package pages;

import static libs.ConfigData.ui;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.TimeUnit;

import libs.CommonUsedWebElements;

import org.apache.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.WebDriverWait;

public class EditProfilePageByDriver {
	static Logger log;	
	static  WebDriver driver;
	CommonUsedWebElements webElements;
	WebDriverWait wait;
	String generatedText;

	public EditProfilePageByDriver(WebDriver driver) throws IOException {
		this.driver = driver;
		webElements = new CommonUsedWebElements(driver);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		wait = new WebDriverWait(driver, 5);
		log = Logger.getLogger(CommonUsedWebElements.class.getName());
	}
	
	
	/*
	 * This method is used to enter some generated text into
	 * textarea "Describe yourself" 
	 */
	
	public void enterTextIntoDescribeYourselfTextarea() throws ClassNotFoundException, IllegalAccessException, InstantiationException, IOException {
		String phrase = "This text was generated: ";
		Date timeNow = new Date();
		SimpleDateFormat format = new SimpleDateFormat("EEE MMM dd HH:mm:ss");

		// Now you can do whatever you need to do with it, for example copy
		// somewhere

		generatedText = phrase + format.format(timeNow);
		log.info("We generated next text - " + generatedText);
		webElements.inputText(generatedText, "EditProfilePage.describeYourselfTextField");
		
	}
	
	
	/*
	 * This method is used to save new setting at "Edit Profile Page"
	 * after user edit his personal data
	 */
	
	public void saveEditedPersonalData() throws ClassNotFoundException, IllegalAccessException, InstantiationException, IOException {
		webElements.clickButton("EditProfilePage.saveButton");
		log.info("Save button was pressed");
	}
	
	/*
	 * This method is used to enter text and click save button
	 * include enterTextIntoDescribeYourselfTextarea() and saveEditedPersonalData()
	 */
	
	public void enterTextIntoIntoDescribeYourselfFieldAndSave() throws ClassNotFoundException, IllegalAccessException, InstantiationException, IOException {
		enterTextIntoDescribeYourselfTextarea();
		saveEditedPersonalData();
	}

	/*
	 * This method is used to return generated text 
	 */
	
	public String getGeneratedText() {
		return generatedText;
	}
	
}
