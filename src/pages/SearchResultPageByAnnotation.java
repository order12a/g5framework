package pages;

import static libs.ConfigData.ui;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import libs.CommonUsedWebElements;

import org.apache.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class SearchResultPageByAnnotation {
	private  WebDriver driver;
	CommonUsedWebElements webElements;
//	WebDriverWait wait;
	static Logger log = Logger.getLogger(LogInPageByDriver.class.getName());

	public SearchResultPageByAnnotation(WebDriver driver) throws IOException {
		this.driver = driver;
		webElements = new CommonUsedWebElements(driver);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		PageFactory.initElements(driver, this);
//		wait = new WebDriverWait(driver, 5);
	}
	
	/**
	 * Here we are locating main elements of Search Result Page
	 */

	//Search field for location input
	
	@FindBy(xpath=".//input[@name='location']")
	private WebElement searchLocationField;
	
	//button that enable additional filters
	
	@FindBy(xpath=".//button[contains(@class, 'show-filters')]")
	private WebElement buttonMoreFilters;
	
	//button that hide additional filters
	
	@FindBy(xpath=".//button[contains(text(), 'Show Listings')]")
	private WebElement buttonShowListings;
	
	//Button that cancel filter by Room Type - Entire Place
	
	@FindBy(xpath=".//label[@id='entire-place-tooltip']/div[1]/input[@type='checkbox']")
	private WebElement buttonRoomTypeX;
	
	//Web elements with results of filter by Entire Place
	
	@FindBy(xpath=".//div[contains(@class, 'listing-location')]/a[1]")
	private WebElement resultsOfFilter;
	
	//Bottom we have a list of additional Filters 
	@FindBy(xpath=".//label[contains(text(), 'Size')]")
	private WebElement labelSize;
	
	@FindBy(xpath=".//label[contains(text(), 'Amenities')]")
	private WebElement labelAmenities;
	
	@FindBy(xpath=".//label[contains(text(), 'Property Type')]")
	private WebElement labelPropertyType;
	
	@FindBy(xpath=".//label[contains(text(), 'Host Language')]")
	private WebElement labelHostLanguage;
	
	@FindBy(xpath=".//label[contains(text(), 'Keywords')]")
	private WebElement labelKeywords;
	
	//Element that enables filter by Room Type - Entire place
	
	@FindBy(xpath=".//label[@id='entire-place-tooltip']/div[1]/input[@type='checkbox']")
	private WebElement checkboxEntirePlace;
	
	
	 /* In this method we verify that text in search fild at this page is equal to 
	 * text enterd in location field at home page
	 */

	public void verifyLocationIsCorrect(String location) throws ClassNotFoundException, IllegalAccessException, InstantiationException, IOException {
		String textFromLocationField = searchLocationField.getText();
		boolean result;
		result = location.equals(textFromLocationField);
		if (result) {
			log.info("Result of verification is - " + result + " Location - " + location + " was found correctly");
		}else{
			log.error("Expected result NOT FOUND");
		}
	}
	
	
	/*
	 * This method is used to press button "Show Filters"
	 */
	
	public void showFilters() throws ClassNotFoundException, IllegalAccessException, InstantiationException, IOException {
		buttonMoreFilters.click();
		log.info("Show Filters button was pressed");
	}
	
	
	/*
	 * This method is used to show all additional filters for host places are displayed
	 */
	
	public void checkAdditionalFiltersArePresent() {
		boolean elementIsPresent = true;
		elementIsPresent = elementIsPresent & labelSize.isDisplayed();
		elementIsPresent = elementIsPresent & labelAmenities.isDisplayed();
		elementIsPresent = elementIsPresent & labelHostLanguage.isDisplayed();
		elementIsPresent = elementIsPresent & labelPropertyType.isDisplayed();
		elementIsPresent = elementIsPresent & labelKeywords.isDisplayed();
		log.info("All additional Filters are present at the page: " + elementIsPresent);
	}
	
	
	/*
	 * This method is used to hide additional filters and show listings
	 */
	
	public void showListings() throws ClassNotFoundException, IllegalAccessException, InstantiationException, IOException {
		buttonShowListings.click();
		log.info("Show Listings button was pressed");
	}
	
	
	/*
	 * This method is used to enable Room Type filter - Entire Place
	 */
	
	public void selectEntirePlaceFilter(String checkboxState) throws ClassNotFoundException, IllegalAccessException, InstantiationException, IOException {
		checkboxEntirePlace.click();
		log.info("Entire Place filter enabled");
	}
	
	
	/*
	 * This method is used to verify that Entire Place listings available
	 */
	
	public void checkEntirePlaceWasFound() throws ClassNotFoundException, IllegalAccessException, InstantiationException, IOException {
		String marker = "Entire";
		
		if (resultsOfFilter.getText().contains(marker)) {
			log.info("Entire place is found. Filter by Entire Place works correctly");
		}else{
			log.error("Error in Filter Room by Entire Place");
		}
	}
	
	
	/*
	 * This method is used disable filter Room Type
	 */
	
	public void roomTypeFilterDisable() throws ClassNotFoundException, IllegalAccessException, InstantiationException, IOException {
		buttonRoomTypeX.click();
		log.info("Filter Room Type by Entire Place was disabled");
	}
	
}
