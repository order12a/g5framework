package pages;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.TimeUnit;

import libs.CommonUsedWebElements;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
	
/**
 * @author order
 * This is class for LDashboardPage of airbnb.com 
 * our test project
 */

public class EditProfilePageByAnnotation {
		
	private  WebDriver driver;
	CommonUsedWebElements webElements;
	static Logger log = Logger.getLogger(LogInPageByDriver.class.getName());
	String generatedText;

	public EditProfilePageByAnnotation(WebDriver driver) throws IOException {
		this.driver = driver;
		webElements = new CommonUsedWebElements(driver);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		PageFactory.initElements(driver, this);
	}
	
	/**
	 * Here we locating all webelements that we need at Edit Profile Page
	 */
	
	@FindBy(xpath=".//div/div/textarea[@id='user_profile_info_about']")
	private WebElement describeYourselfTextField;
	
	@FindBy(xpath=".//form[@id='update_form']/button[@type='submit']")
	private WebElement saveButton;
	
	
	/*
	 * This method is used to enter new text into field Describe Yourself
	 */
	
	public void changeDescribeYouselfInformation() {
		String phrase = "This text was generated: ";
		Date timeNow = new Date();
		SimpleDateFormat format = new SimpleDateFormat("EEE MMM dd HH:mm:ss");
		generatedText = phrase + format.format(timeNow);
		
		describeYourselfTextField.clear();
		describeYourselfTextField.sendKeys(generatedText);
		log.info("We generated and enterd next text - " + generatedText);
	}
	
	
	/*
	 * This method is used to click Save button
	 */
	
	public void clickSaveButton() {
		saveButton.click();
		log.info("Save button was clicked");
	}
	
	
	/*
	 * this method is used to enter text into describeYourselfTextField and click 
	 * Save button
	 */
	
	
	public void enterTextIntoIntoDescribeYourselfFieldAndSave() {
		changeDescribeYouselfInformation();
		clickSaveButton();
	}
	
}
