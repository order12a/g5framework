package pages;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import libs.CommonUsedWebElements;

import org.apache.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

public class ViewUserProfilePageByDriver {
	static Logger log;

	
	static  WebDriver driver;
	CommonUsedWebElements webElements;
	WebDriverWait wait;

	public ViewUserProfilePageByDriver(WebDriver driver) throws IOException {
		this.driver = driver;
		webElements = new CommonUsedWebElements(driver);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		wait = new WebDriverWait(driver, 5);
		log = Logger.getLogger(CommonUsedWebElements.class.getName());
	}
	
	/*
	 * This method is used to verify that expected text is present at
	 * target element
	 */
	
	public void checkText(String generatedText) throws ClassNotFoundException, IllegalAccessException, InstantiationException, IOException {
		webElements.verifyTextPresent("ViewProfilePage.textAboutUserField", generatedText);
		log.info("Presense of Text: '" + generatedText + "' was verified");
	}
}
