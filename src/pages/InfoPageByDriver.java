package pages;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import libs.CommonUsedWebElements;

import org.apache.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

public class InfoPageByDriver {

	static Logger log;
	static WebDriver driver;
	CommonUsedWebElements webElements;
	WebDriverWait wait;

	public InfoPageByDriver(WebDriver driver) throws IOException {
		this.driver = driver;
		webElements = new CommonUsedWebElements(driver);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		wait = new WebDriverWait(driver, 5);
		log = Logger.getLogger(CommonUsedWebElements.class.getName());
	}
	
	/*
	 * This method is used to check that illustrations for page Rental Apartments are present 
	 */
	
	public void checkRentalApartmentsIllustrations() throws ClassNotFoundException, IllegalAccessException, InstantiationException, IOException {
		
		webElements.verifyElementIsPresent("InfoPage.rentaIlllustrations");
	}
	
	
	/*
	 * This method is used to navigate to how-to-travel page
	 */
	
	public void pressRentApartments() throws ClassNotFoundException, IllegalAccessException, InstantiationException, IOException {
		webElements.clickButton("InfoPage.howToRentApartments");
	}
	
	
	/*
	 * This method is used to check that illustrations for page Rent Apartments are present(how=to-travel) 
	 */
	
	public void checkRentApartmentsIllustrations() throws ClassNotFoundException, IllegalAccessException, InstantiationException, IOException {
		
		webElements.verifyElementIsPresent("InfoPage.howToRentIllustration");
	}
	
	
	/*
	 * This element is used to navigate to How It Works page
	 */
	
	public void pressHowItWorks() throws ClassNotFoundException, IllegalAccessException, InstantiationException, IOException {
		webElements.clickButton("InfoPage.howItWorks");
	}
	
	
	/*
	 * This method is used to check that illustrations for page How It Works are present
	 */
	
	public void checkHowItWorksIllustrations() throws ClassNotFoundException, IllegalAccessException, InstantiationException, IOException {
		
		webElements.verifyElementIsPresent("InfoPage.howItWorksIllustration");
	}
}











