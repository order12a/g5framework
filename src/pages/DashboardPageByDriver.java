package pages;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import libs.CommonUsedWebElements;

import org.apache.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

public class DashboardPageByDriver {
	static Logger log;

	
	static  WebDriver driver;
	CommonUsedWebElements webElements;
	WebDriverWait wait;

	public DashboardPageByDriver(WebDriver driver) throws IOException {
		this.driver = driver;
		webElements = new CommonUsedWebElements(driver);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		wait = new WebDriverWait(driver, 5);
		log = Logger.getLogger(CommonUsedWebElements.class.getName());
	}
	
	
	/*
	 * This method is used to click on Edit Profile link
	 */
	
	public void clickEditProfile() throws ClassNotFoundException, IllegalAccessException, InstantiationException, IOException {
		webElements.clickButton("DashboardPage.editProfileLink");
		log.info("Edit profile link was clicked.");
	}
}
