package pages;

import java.io.IOException;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
	
/**
 * @author order
 * This is class for LoggedInLandingPage of airbnb.com 
 * our test project
 */

public class LoggedInLandingPage {
		
	static  WebDriver driver;

	public LoggedInLandingPage(WebDriver driver) throws IOException {
		this.driver = driver;
	}
	
	/**
	 * Here we locating main elements of LoggedInLandingPage
	 */
	
	//with annotation
	@FindBy(xpath=".//a[contains(@class, 'inbox-icon')]/i")
	private WebElement mail;
	
	/*
	 * In this method we check element photoIcon is present at the page 
	 */
	public void checkPhotoIcon() {
		WebElement photoIconAlternative;
		photoIconAlternative = driver.findElement(By.xpath(".//div[contains(@class, 'user-profile-image')]"));
	}
	
	//with annotation
	@FindBy(xpath=".//h1[contains(text(), 'Welcome home')]")
	private WebElement photoIcon;
		
	/*
	 * This method is used to check that mail element is present
	 */
	public void checkMailPresent() {
		WebElement mailAlternative;
		mailAlternative = driver.findElement(By.xpath(".//a[contains(@class, 'inbox-icon')]/i"));
	}
		
}
