package pages;

import java.io.IOException;


import java.util.concurrent.TimeUnit;

import libs.CommonUsedWebElements;
import static libs.ConfigData.ui;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class RegistrationPageByAnnotation {
	
	/**
	 * This page for registering new users described by annotations
	 */
	
	private WebDriver driver;
	CommonUsedWebElements webElements;
	static final Logger logger = Logger.getLogger(CommonUsedWebElements.class.getName());
	
	public RegistrationPageByAnnotation(WebDriver driver) throws IOException {
		this.driver = driver;
		webElements = new CommonUsedWebElements(driver);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		 PageFactory.initElements(driver, this);
	}
	
	/*
	 * Input field for First Name
	 */
	
	@FindBy(xpath = ".//input[@name='user[first_name]']")
	private WebElement firstNameField;
	
	/*
	 * Input field for Last Name
	 */
	
	@FindBy(xpath = ".//input[contains (@placeholder, 'Last name')]")
	private WebElement lastNameField;
	
	/*
	 * Input field for email address
	 */
	
	@FindBy(xpath = ".//input[@name='user[email]']")
	private WebElement  emailAddressField;
	
	/*
	 * Input filed for user's password 
	 */
	
	@FindBy(xpath = ".//input[@name='user[password]']")
	private WebElement passwordField;
	
	/*
	 * Input field for user's confirm password
	 */
	
	@FindBy(xpath = ".//input[@name='user[password_confirmation]']")
	private WebElement confirmPasswordField;
	
	/*
	 * Field for checkbox
	 */
	
	@FindBy(xpath = ".//label[@class='checkbox']/input[@type='checkbox']")
	private WebElement checkboxField;
	
	/*
	 * Sign Up button description
	 */
	
	@FindBy(xpath = ".//form[@id='user_new']/div[2]/button[@type='submit']")
	private WebElement signUpButton;
	
	/*
	 * This method is used to input First Name
	 */
	
	public void inputLastName(String lastName){
		lastNameField.clear();
		lastNameField.sendKeys(lastName);
	}
	
	/*
	 * This method is used to input Last Nname
	 */
	
	public void inputFirstName(String firstName) {
		firstNameField.clear();
		firstNameField.sendKeys(firstName);
	}
	
	/*
	 * This method is used to input email
	 */
	
	public void inputEmail(String email) {
		emailAddressField.clear();
		emailAddressField.sendKeys(email);
	}
	
	/*
	 * This method is used to enter data into password field
	 */
	
	public void inputPassword(String password) {
		passwordField.clear();
		passwordField.sendKeys(password);
	}

	/*
	 * This method is used enter data in confirm password field
	 */
	
	public void inputConfirmPassword(String confirmPassword) {
		confirmPasswordField.clear();
		confirmPasswordField.sendKeys(confirmPassword);
	}

	/*
	 * This method is used set state of checkbox before registration
	 */
	
	public void setCheckboxState(String checkboxState) {
		
		if (checkboxField.isSelected() && checkboxState.equals("YES")) {
			System.out.println("Checkbox is selected");
		} else if(!checkboxField.isSelected() && checkboxState.equals("NO")){
			System.out.println("Checkbox is not selected");
		} else if(checkboxField.isSelected() && checkboxState.equals("NO")){
			System.out.println("Here we deselect checkbox");
			checkboxField.click();
		} else if(!checkboxField.isSelected() && checkboxState.equals("YES")){
			System.out.println();
			checkboxField.click();
		}
	}
	
	/*
	 * This method is used to press Sign Up button
	 */
	
	public void inputSignUp() {
		signUpButton.click();
	}
	
	/*
	 * This method is used to register new user
	 */
	
	public void registerNewUser(String firstName, String lastName, String email, String password, String passwordConfirm, String checkBoxState) {
		
		inputFirstName(firstName);
		inputLastName(lastName);
		inputEmail(email);
		inputPassword(password);
		inputConfirmPassword(passwordConfirm);
		setCheckboxState(checkBoxState);
		inputSignUp();
	}
}
