package pages;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import libs.CommonUsedWebElements;

import org.apache.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;

public class HomePageByAnnotation {
	private  WebDriver driver;
	CommonUsedWebElements webElements;
	static Logger log = Logger.getLogger(LogInPageByDriver.class.getName());

	public HomePageByAnnotation(WebDriver driver) throws IOException {
		this.driver = driver;
		webElements = new CommonUsedWebElements(driver);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		PageFactory.initElements(driver, this);
	}
	
	/**
	 * Here we are locating main elements of Home Page
	 */
	
	@FindBy(xpath=".//h1[contains(text(), 'Welcome home')]")
	private WebElement welcomeHeader;
	
	@FindBy(xpath=".//a[contains(@class, 'how-it-works')]")
	private WebElement howItWorksButton;
	
	@FindBy(xpath=".//li[@id='login']/a[contains(text(), 'Log In')]")
	private WebElement logInLink;

	@FindBy(xpath=".//li[@id='sign_up']/a[contains(text(), 'Sign Up')]")
	private WebElement signUpLink;

	@FindBy(xpath=".//a[@href='/signup_login?sm=2']")
	private WebElement signUpByEmailLink;
	
	@FindBy(xpath=".//form[@id='search_form']/div/input[@id='location']")
	private WebElement locationField;

	@FindBy(xpath=".//form[@id='search_form']/input[@id='checkin']")
	private WebElement checkinField;

	@FindBy(xpath=".//form[@id='search_form']/input[@id='checkout']")
	private WebElement checkoutField;

	@FindBy(xpath=".//form[@id='search_form']/div/select[@id='guests']")
	private WebElement numberOfGuestsField;

	@FindBy(xpath=".//form[@id='search_form']/button[@id='submit_location']")
	private WebElement searchButton;

	@FindBy(xpath=".//div[3]/p/a[contains(text(), 'Learn more')]")
	private WebElement learnMoreLink;

	@FindBy(xpath=".//li[@id='inbox-item']/a/i[contains(@class, 'icon-envelope')]")
	private WebElement envelopIcon;
	
	@FindBy(xpath=".//div[contains (@class, 'user-profile-image')]")
	private WebElement userProfileImage;
	
	@FindBy(xpath=".//a[@id='header-avatar-trigger']/span[contains(@class, 'value_name')]")
	private WebElement userProfileName;
	
	@FindBy(xpath=".//li/a[@id='header-logout']")
	private WebElement logOutLinkLocator;
	
	@FindBy(xpath=".//ul[contains(@class, 'help-menu')]/li[contains(@class, 'help-menu-container')]/a[@id='header-help-trigger']")
	private WebElement helpMenuLocator;
	
	@FindBy(xpath=".//div[contains(@class, 'airbnb-header')]/a")
	private WebElement homePageLink;
	
	/*
	 *This methos is used to verify that user is at HomePage 
	 */
	
	public boolean verifyHomePage(){
		if (welcomeHeader.isDisplayed() && howItWorksButton.isDisplayed()) {
			log.info("We are at Home Page");
			return true;
		} else {
			log.info("This is not Home Page");
			return false;
		}
	}
	
	/*
	 * This method is used to click sign up link
	 */
	
	public void clickSignUpLink(){
		signUpLink.click();
		log.info("Sign Up link was clicked");
	}
	
	/*
	 * This method is used to click on sign up by email link
	 */
	
	public void signUpByEmail(){
		signUpByEmailLink.click();
		log.info("Sign Up By Email button was clicked");
	}
	
	/*
	 * This methos is used to click on login link
	 */
	
	public void clickOnLogInLink(){
		logInLink.click();
		log.info("Log In link was clicked");
	}
	
	/*
	 * This method is used to enter location address into field "Where do you want to go?"
	 */
	
	public void enterLocationAddress(String location){
		locationField.clear();
		locationField.sendKeys(location);
		log.info("Location address: " + location + " was enterd into Location Field");
	}
	
	/*
	 * This method is used to enter checkin date by input text into checkin field
	 * format xx/xx/xxxx
	 */
	
	public void enterCheckinDate(String checkinDate) throws ClassNotFoundException, IllegalAccessException, InstantiationException, IOException {
		checkinField.clear();
		checkinField.sendKeys(checkinDate);
		log.info(checkinDate + " checkin date was entered");
	}
	
	/*
	 * This method is used to enter checkout date using text input into field chceckout
	 * format xx/xx/xxxx
	 */
	
	public void enterCheckoutDate(String checkoutDate) throws ClassNotFoundException, IllegalAccessException, InstantiationException, IOException {
		checkoutField.clear();
		checkoutField.sendKeys(checkoutDate);
		log.info(checkoutDate + " checkout date was entered");
	}
	
	/*
	 * This method is used to select number of guests from dropdown list
	 * Please make Parse Int action before enter numberOfGuests parametr
	 */
	
	public void selectNumberOfGuests(int numberOfGuests) throws ClassNotFoundException, InstantiationException, IllegalAccessException, IOException {
		String guestValue = "";
		if (numberOfGuests == 1) {
			guestValue =  numberOfGuests + " Guest";
		}else if(numberOfGuests > 1 && numberOfGuests < 16){
			guestValue = numberOfGuests + " Guests";
		}else if(numberOfGuests >= 16){
			guestValue = numberOfGuests + "+ Guests";
		}
		
		new Select(numberOfGuestsField).selectByVisibleText(guestValue);
		
		log.info("Number of guests - " + guestValue);
	}
	
	
	
	
	
	
}






