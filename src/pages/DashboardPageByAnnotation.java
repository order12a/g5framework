package pages;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import libs.CommonUsedWebElements;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
	
/**
 * @author order
 * This is class for LDashboardPage of airbnb.com 
 * our test project
 */

public class DashboardPageByAnnotation {
		
	private  WebDriver driver;
	CommonUsedWebElements webElements;
//	WebDriverWait wait;
	static Logger log = Logger.getLogger(LogInPageByDriver.class.getName());

	public DashboardPageByAnnotation(WebDriver driver) throws IOException {
		this.driver = driver;
		webElements = new CommonUsedWebElements(driver);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		PageFactory.initElements(driver, this);
//		wait = new WebDriverWait(driver, 5);
	}
	
	/**
	 * Here we are locating main elements of Page
	 */
	
	@FindBy(xpath=".//li/a[@id='edit-profile']")
	private WebElement editProfileLink;
	
	
	/*
	 * This method is used to click on Edit Profile link
	 */
	
	public void clickEditProfile() {
		editProfileLink.click();
	}
}
