package pages;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import libs.CommonUsedWebElements;

import org.apache.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class InfoPageByAnnotation {
	private  WebDriver driver;
	CommonUsedWebElements webElements;

	static Logger log = Logger.getLogger(LogInPageByDriver.class.getName());

	public InfoPageByAnnotation(WebDriver driver) throws IOException {
		this.driver = driver;
		webElements = new CommonUsedWebElements(driver);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		PageFactory.initElements(driver, this);
	}
	
	/**
	 * Here we are locating main elements of Info Page
	 */
	
	//Rental Apartments link and atrributes
	
	@FindBy(xpath=".//li[@id='sb-0-0-2']/a[@href='/help/getting-started/how-to-host']")
	private WebElement aboutRentalApartments;
	
	@FindBy(xpath=".//div[contains(@class, 'illustration')]")
	private WebElement rentaIlllustrations;
	
	//How To Rent Apartments and attributes
	
	@FindBy(xpath=".//li[@id='sb-0-0-1']/a[@href='/help/getting-started/how-to-travel']")
	private WebElement howToRentApartments;
	
	@FindBy(xpath=".//ol[contains(@class, 'getting-started-list')]/li[1]/div/div[1]/div[contains(@class, 'illustration')]")
	private WebElement howToRentIllustration;
	
	//How It Works and attributes
	
	@FindBy(xpath=".//li[@id='sb-0-0-0']/a[@href='/help/getting-started/how-it-works']")
	private WebElement howItWorks;
	
	@FindBy(xpath=".//ol[contains(@class, 'getting-started-list')]/li[1]/div/div[1]/div[2]")
	private WebElement howItWorksIllustration;

	
	
}