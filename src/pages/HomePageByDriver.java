package pages;

import java.io.IOException;
import java.sql.SQLException;
import java.util.concurrent.TimeUnit;

import libs.CommonUsedWebElements;
import static libs.ConfigData.ui;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import database.Database;
import database.DatabaseData;

public class HomePageByDriver {
	static  WebDriver driver;
	static Logger log;
	CommonUsedWebElements webElements;
	Database  database;
	DatabaseData dbData;

	public HomePageByDriver(WebDriver driver, Database database) throws IOException {
		this.driver = driver;
		this.database = database;
		dbData = new DatabaseData(database);
		webElements = new CommonUsedWebElements(driver);
		log = Logger.getLogger(CommonUsedWebElements.class.getName());
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	}
		
	public HomePageByDriver(WebDriver driver) throws IOException {
		this.driver = driver;

		webElements = new CommonUsedWebElements(driver);
		log = Logger.getLogger(CommonUsedWebElements.class.getName());
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	}
	
	/*
	 * This method is use to show how to work with database:
	 * get an id by login
	 * !only example!
	 */
	
	public void inputIdIntoSomeField(String login) throws SQLException {
		String id;
		id = dbData.selectIdByName(login);
		driver.findElement(By.xpath("xpath")).sendKeys(id);
	}
	
	/*
	 *This methos is used to verify that user is at HomePage 
	 */
	
	public boolean verifyHomePage(String welcomeHeaderLocator, String howItWorksButtonLoctor) throws ClassNotFoundException, IllegalAccessException, InstantiationException, IOException {
		WebElement welcomeHeader = driver.findElement(ui(welcomeHeaderLocator));
		WebElement howItWorksButton = driver.findElement(ui(howItWorksButtonLoctor));
		
		if (welcomeHeader.isDisplayed() && howItWorksButton.isDisplayed()) {
			log.info("We are at Home Page");
			return true;
		} else {
			log.info("This is not Home Page");
			return false;
		}
	}
	
	/*
	 * This method is used to click sign up link
	 */
	
	public void clickSignUpLink(String signUpLinkLocator) throws ClassNotFoundException, IllegalAccessException, InstantiationException, IOException {
		webElements.clickButton(signUpLinkLocator);
	}
	
	/*
	 * This methos is used to click on login link
	 */
	
	public void clickOnLogInLink() throws ClassNotFoundException, IllegalAccessException, InstantiationException, IOException {
		webElements.clickButton("HomePage.logInLink");
	}
	
	/*
	 * This method is used to click on sign up by email link
	 */
	
	public void signUpByEmail(String signUpByEmailLocator) throws ClassNotFoundException, IllegalAccessException, InstantiationException, IOException {
		webElements.clickButton(signUpByEmailLocator);
	}
	
	/*
	 * This method is used to enter location address into field "Where do you want to go?"
	 */
	
	public void enterLocationAddress(String location) throws ClassNotFoundException, IllegalAccessException, InstantiationException, IOException {
		webElements.inputText(location, "HomePage.locationField");
		
	}
	
	/*
	 * This method is used to enter checkin date by input text into checkin field
	 * format xx/xx/xxxx
	 */
	
	public void enterCheckinDate(String checkinDate) throws ClassNotFoundException, IllegalAccessException, InstantiationException, IOException {
		webElements.inputText(checkinDate, "HomePage.checkinField");
		log.info(checkinDate + " checkin date was entered");
	}
	
	/*
	 * This method is used to enter checkout date using text input into field chceckout
	 * format xx/xx/xxxx
	 */
	
	public void enterCheckoutDate(String checkoutDate) throws ClassNotFoundException, IllegalAccessException, InstantiationException, IOException {
		webElements.inputText(checkoutDate, "HomePage.checkoutField");
		log.info(checkoutDate + " checkout date was entered");
	}
	
	/*
	 * This method is used to select number of guests from dropdown list
	 * Please make Parse Int action before enter numberOfGuests parametr
	 */
	
	public void selectNumberOfGuests(int numberOfGuests) throws ClassNotFoundException, InstantiationException, IllegalAccessException, IOException {
		String guestValue = "";
		if (numberOfGuests == 1) {
			guestValue =  numberOfGuests + " Guest";
		}else if(numberOfGuests > 1 && numberOfGuests < 16){
			guestValue = numberOfGuests + " Guests";
		}else if(numberOfGuests >= 16){
			guestValue = numberOfGuests + "+ Guests";
		}
		
		webElements.selectFromListByClick(guestValue, "HomePage.numberOfGuestsField");
		
		log.info("Number of guests - " + guestValue);
	}
	
	/*
	 * This method is used to press "Search" button
	 */
	
	public void pressSearchButton() throws ClassNotFoundException, IllegalAccessException, InstantiationException, IOException {
		webElements.clickButton("HomePage.searchButton");
		log.info("Search button was pressed");
	}
	
	/*
	 * This method is ued to search new rent place
	 */
	
	public void searchNewRentPlace(String location, String checkinDate, String checkoutDate, int numberOfGuests) throws ClassNotFoundException, IllegalAccessException, InstantiationException, IOException {
		enterLocationAddress(location);
		enterCheckinDate(checkinDate);
		enterCheckoutDate(checkoutDate);
		selectNumberOfGuests(numberOfGuests);
		pressSearchButton();
	}
	
	/*
	 * This method is used to return to HomePage from 
	 * either pages
	 */
	
	public void returnToHomePage() throws ClassNotFoundException, IllegalAccessException, InstantiationException, IOException {
		webElements.clickButton("EveryPage.homePageLink");
	}
	
	/*
	 * This method is used to focus on "Help" element and
	 * capture that list shows at this moment using screenshot method
	 */
	
	public void focusOnHelp() throws ClassNotFoundException, IllegalAccessException, InstantiationException, IOException, InterruptedException {
		webElements.focusOnElement("EveryPage.helpMenuLocator");
		webElements.makeScreenShot("C:\\selenium_and_java\\seleniumAirbnbTest\\", "jpg");
		webElements.waitForSomeTime(1000);
		webElements.focusOnElement("EveryPage.homePageLink");
	}
	
	
	/*
	 * This method is used to click on "How it Works" button
	 */
	
	public void clickHowItWorks() throws ClassNotFoundException, IllegalAccessException, InstantiationException, IOException {
		webElements.clickButton("HomePage.howItWorksButton");
	}
	
	
	/*
	 * This method is used to click on "Learn More" link
	 */
	
	public void clickLearnMore() throws ClassNotFoundException, IllegalAccessException, InstantiationException, IOException {
		webElements.clickButton("HomePage.learnMoreLink");
	}
	
	
	/*
	 * This method is used to Log Out
	 */
	
	public void userLogOut() throws ClassNotFoundException, IllegalAccessException, InstantiationException, IOException, InterruptedException {
		webElements.logOut("EveryPage.userProfileName", "Log Out");
	}
	
	
	/*
	 * This method is used to check that home page succesfully opened
	 */
	
	public void verifyHomePageOpened(String url, String title) throws ClassNotFoundException, InstantiationException, IllegalAccessException, IOException {
		if(webElements.open(url, title)) {
			log.info("Connection is OK");
		} else {
			log.error("Unable set up connection");
		}
	}
}










