package pages;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import libs.CommonUsedWebElements;

import org.apache.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class ViewUserProfilePageByAnnotation {
	private  WebDriver driver;
	CommonUsedWebElements webElements;
	static Logger log = Logger.getLogger(LogInPageByDriver.class.getName());

	public ViewUserProfilePageByAnnotation(WebDriver driver) throws IOException {
		this.driver = driver;
		webElements = new CommonUsedWebElements(driver);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		PageFactory.initElements(driver, this);
	}
	
	/**
	 * Here we are locating web elements of View Profile Page
	 */
	
	//Field that contains information about user in a free text form
	
	@FindBy(xpath=".//div[contains(@class, 'page-container')]/div[@class='row']/div[@class='col-9']/div[contains(@class, 'row-space-4')]/div[@class='row-space-top-2']/p")
	private WebElement textAboutUserField;
	
	/*
	 * This method is used to verify that expected text is present at
	 * target element
	 */
	
	public void checkText(String generatedText) throws ClassNotFoundException, IllegalAccessException, InstantiationException, IOException {
		if ((textAboutUserField.getText()).equals(generatedText)) {
			log.info("Presense of Text: '" + generatedText + "' was verified");
		}else{
			log.error("Expected text: " + generatedText + " is not present at the page");
		}	
	}
}
