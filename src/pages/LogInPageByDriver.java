package pages;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import libs.CommonUsedWebElements;
import static libs.ConfigData.ui;

import org.apache.log4j.Logger;


import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
	
 /**
 * @author order
 * This is class for LogIn Page of airbnb.com 
 * our test project
 */

public class LogInPageByDriver {
	static Logger log;

		
	static  WebDriver driver;
	CommonUsedWebElements webElements;
	WebDriverWait wait;

	public LogInPageByDriver(WebDriver driver) throws IOException {
		this.driver = driver;
		webElements = new CommonUsedWebElements(driver);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		wait = new WebDriverWait(driver, 5);
		log = Logger.getLogger(CommonUsedWebElements.class.getName());
	}
	
	/**
	 * Here we are locating main elements of Page and
	 * doing basic operation with them
	 * @throws IOException 
	 * @throws InstantiationException 
	 * @throws IllegalAccessException 
	 * @throws ClassNotFoundException 
	 * @throws InterruptedException 
	 */
	
	/*
	 * This method is used to input login into login field
	 */
	
	public void inputLogin(String email) throws IOException, ClassNotFoundException, IllegalAccessException, InstantiationException, InterruptedException {
//		WebElement logInAlternative;
//		logInAlternative = driver.findElement(By.xpath(".//form[contains (@class, 'login-form')]/button[@type='submit']"));	
//		logInAlternative.clear();
//		logInAlternative.sendKeys(login);
//		
//		here we enter some text into some field
//		wait.until(ExpectedConditions.visibilityOfElementLocated(ui("LoginPage.emailField")));
		webElements.inputText(email, "LoginPage.emailField");
//		wait.until(ExpectedConditions.textToBePresentInElementLocated(ui("LoginPage.emailField"), email));
		log.info(email + "email was inputed into LoginPage.emailField");
		
//		webElements.waitForSomeTime(1000);
	}
		
	/*
	 * This method is used to input password into password field
	 */
	
	public void inputPassword(String pass) throws IOException, ClassNotFoundException, IllegalAccessException, InstantiationException {
//		WebElement password;
//		password = driver.findElement(By.xpath(".//input[@name='password']"));
//		password.clear();
//		password.sendKeys(pass);
//		
//		here we enter password into password field
		webElements.inputText(pass, "LoginPage.passwordField");
	}
	
	/*
	 *  public void rememberCheckbox() {
  		WebElement rememberCheckbox;
  		rememberCheckbox = driver.findElement(By.xpath(".//*[@class='area margin10_0']/label[1]"));
  		rememberCheckbox.click();
 		}
	 */
		
	/*
	 *  public void clickForgotPassLink() {
  		WebElement forgotPassLink;
  		forgotPassLink = driver.findElement(By.xpath(".//*[@id='newpassword']/span"));
  		forgotPassLink.click();
 		}
	 */
	
	
	/*
	 * This method is used to set state of checkbox Remember me
	 * as checked
	 */
	
	public void selectCheckboxChecked(String checkboxState) throws IOException, ClassNotFoundException, IllegalAccessException, InstantiationException {
		webElements.selectCheckBox(checkboxState, "LoginPage.checkboxField");
	}
	
	
	/*
	 * This method is used to find that button Log In is present
	 * at the page and to log in your account using your credentials
	 */
	
	public void pressLogInButton() throws IOException, ClassNotFoundException, IllegalAccessException, InstantiationException {
//		WebElement logInButton;
//		logInButton = driver.findElement(By.xpath(".//button[contains(text(), 'Log In')]"));
//		logInButton.click();
		
		//here we press Log In button
		webElements.clickButton("LoginPage.logInButton");
	}

	/*
	 * This method is used  login user
	 */
	
	public void loginUser(String login, String password, String checkboxState) throws IOException, ClassNotFoundException, IllegalAccessException, InstantiationException, InterruptedException {
//		webElements.waitForSomeTime(2000);
		inputLogin(login);
		inputPassword(password);
		selectCheckboxChecked(checkboxState);
		pressLogInButton();
	}
	
	/*
	 * 
	 */
	public void logExample(String text) {
		  log.info(text+" was inputed into LoginPage.emailField");
		  log.debug("DEBUG message");
		  log.error("ERROR message");
		  log.warn("WARNING message");
		 }
	
	/*
	 * This method is used to log in using facebook
	 */
	
	public void switcher() throws ClassNotFoundException, IllegalAccessException, InstantiationException, IOException{
		  String parentHandle = driver.getWindowHandle(); // get the current window handle
		  driver.findElement(By.xpath(".//*[@title='Facebook']")).click(); // click some link that opens a new window

		  for (String winHandle : driver.getWindowHandles()) {
		      driver.switchTo().window(winHandle); // switch focus of WebDriver to the next found window handle (that's your newly opened window)
		  }
		  
		  webElements.inputText("noob", "Fb.login");
		  
		  //code to do something on new window

		  //driver.close(); // close newly opened window when done with it
		  driver.switchTo().window(parentHandle); // switch back to the original window
		 }
}
