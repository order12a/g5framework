package pages;

import static libs.ConfigData.ui;

import java.io.IOException;
import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

import libs.CommonUsedWebElements;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.WebDriverWait;

/**
 * @author order This is class for Results Page of airbnb.com our test project
 */

public class SearchResultPageByDriver {

	static Logger log;
	static WebDriver driver;
	CommonUsedWebElements webElements;
	WebDriverWait wait;

	public SearchResultPageByDriver(WebDriver driver) throws IOException {
		this.driver = driver;
		webElements = new CommonUsedWebElements(driver);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		wait = new WebDriverWait(driver, 5);
		log = Logger.getLogger(CommonUsedWebElements.class.getName());
	}

	/*
	 * In this method we verify that text in search fild at this page is equal to 
	 * text enterd in location field at home page
	 */

	public void verifyLocationIsCorrect(String location) throws ClassNotFoundException, IllegalAccessException, InstantiationException, IOException {
		boolean locationVerifyResult = webElements.verifyTextPresent("SearchResultPage.searchLocationField", location);
		log.info("Result of verification is - "  + locationVerifyResult + " Location - " + location + " was found correctly");
	}
	
	
	/*
	 * This method is used to press button "Show Filters"
	 */
	
	public void showFilters() throws ClassNotFoundException, IllegalAccessException, InstantiationException, IOException {
		webElements.clickButton("SearchResultPage.buttonMoreFilters");
		log.info("Show Filters button was pressed");
	}
	
	
	/*
	 *  This method is used to show all additional filters for host places are displayed
	 */
	
	public void checkAdditionalFiltersArePresent() throws ClassNotFoundException, IllegalAccessException, InstantiationException, IOException {
		ArrayList<String> listOfElementsLocators = new ArrayList<>();
		listOfElementsLocators.add("SearchResultPage.labelSize");
		listOfElementsLocators.add("SearchResultPage.labelAmenities");
		listOfElementsLocators.add("SearchResultPage.labelPropertyType");
		listOfElementsLocators.add("SearchResultPage.labelHostLanguage");
		listOfElementsLocators.add("SearchResultPage.labelKeywords");
		boolean presenseOfFilters = webElements.verifyElementsArePresent(listOfElementsLocators);
		log.info("All additional Filters are present at the page: " + presenseOfFilters);
	}
	
	
	/*
	 * This method is used to hide additional filters and show listings
	 */
	
	public void showListings() throws ClassNotFoundException, IllegalAccessException, InstantiationException, IOException {
		webElements.clickButton("SearchResultPage.buttonShowListings");
		log.info("Show Listings button was pressed");
	}
	
	
	/*
	 * This method is used to enable Room Type filter - Entire Place
	 */
	
	public void selectEntirePlaceFilter(String checkboxState) throws ClassNotFoundException, IllegalAccessException, InstantiationException, IOException {
		webElements.selectCheckBox(checkboxState, "SearchResultPage.checkboxEntirePlace");
		log.info("Entire Place filter enabled");
	}
	
	
	/*
	 * This method is used to verify that Entire Place listings available
	 */
	
	public void checkEntirePlaceWasFound() throws ClassNotFoundException, IllegalAccessException, InstantiationException, IOException {
		String marker = "Entire";
		WebElement targetElement = driver.findElement(ui("SearchResultPage.resultOfFilter"));
		if (targetElement.getText().contains(marker)) {
			log.info("Entire place is found. Filter by Entire Place works correctly");
		}else{
			log.error("Error in Filter Room by Entire Place");
		}
	}
	
	
	/*
	 * This method is used disable filter Room Type
	 */
	
	public void roomTypeFilterDisable() throws ClassNotFoundException, IllegalAccessException, InstantiationException, IOException {
		webElements.clickButton("SearchResultPage.buttonRoomTypeX");
		log.info("Filter Room Type by Entire Place was disabled");
	}
}






