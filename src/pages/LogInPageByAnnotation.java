package pages;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import libs.CommonUsedWebElements;
import static libs.ConfigData.ui;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.WebDriverWait;
	
 /**
 * @author order
 * This is class for LogIn Page of airbnb.com 
 * our test project
 */

public class LogInPageByAnnotation {
		
	private  WebDriver driver;
	CommonUsedWebElements webElements;
	static Logger log = Logger.getLogger(LogInPageByDriver.class.getName());

	public LogInPageByAnnotation(WebDriver driver) throws IOException {
		this.driver = driver;
		webElements = new CommonUsedWebElements(driver);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		PageFactory.initElements(driver, this);
	}
	
	/**
	 * Here we are locating main elements of Login Page
	 */
	
	//Email Address field definition with annotation
	
	@FindBy(xpath=".//input[@id='signin_email']")
	private WebElement emailAddressField;
	
	//Password field definition with annotation
	
	@FindBy(xpath=".//input[@name='password']")
	private WebElement passwordField;
	
	//Checkbox remember Me field
	
	@FindBy(xpath=".//label[@class='checkbox remember-me']/input[@id='remember_me2']")
	private WebElement checkboxField;
	
	//Sign Up button definition with annotation
	
	@FindBy(xpath=".//form[contains (@class, 'login-form')]/button[@type='submit']")
	private WebElement logInButton;
	
	
	/*
	 * This method is used to input login into login field
	 */
	
	public void inputEmail(String email) throws InterruptedException {
		webElements.waitForSomeTime(5000);
		log.info("In method that enter email, before clear");
		emailAddressField.clear();
		log.debug("In method that enter email, after clear and tag name is: " + emailAddressField.getTagName());
		emailAddressField.sendKeys(email);
	}
	
	
	/*
	 * This method is used to input password into password field
	 */
	
	public void inputPassword(String password) throws InterruptedException {
		webElements.waitForSomeTime(5000);
		passwordField.clear();
		passwordField.sendKeys(password);
	}
	
	/*
	 * This method is used to set checkbox "Remember me" state
	 */
	
	public void setCheckboxState(String checkboxState) throws InterruptedException {
		webElements.waitForSomeTime(5000);
		if (checkboxField.isSelected() && checkboxState.equals("YES")) {
			log.info("Checkbox is selected by default");
		} else if(!checkboxField.isSelected() && checkboxState.equals("NO")){
			log.info("Checkbox is not selected by default");
		} else if(checkboxField.isSelected() && checkboxState.equals("NO")){
			checkboxField.click();
			log.info("Checkbox is selected and we uncheck this checkbox");
		} else if(!checkboxField.isSelected() && checkboxState.equals("YES")){
			checkboxField.click();			
		}
	}
	
	/*
	 * This method is used to find that button Log In is present
	 * at the page and to log in your account using your credentials
	 */
	
	public void pressLogInButton() throws InterruptedException {
		webElements.waitForSomeTime(5000);
		logInButton.click();
	}
	
	/*
	 * This metod is used to login user
	 */
	
	public void userLogIn(String email, String password, String checkBoxState) throws InterruptedException {
		inputEmail(email);
		inputPassword(password);
		setCheckboxState(checkBoxState);
		pressLogInButton();
	}
}
