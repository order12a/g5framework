package pages;
//package pages;
//
//import org.junit.Assert;
//import org.openqa.selenium.WebDriver;
//import org.openqa.selenium.WebElement;
//import org.openqa.selenium.support.FindBy;
//import org.openqa.selenium.support.PageFactory;
// 
//public class RegistrationPage {
// 
//    private static String URL_MATCH = "registration";
// 
//    private WebDriver driver;
// 
//    /**
//     * �����
//     */
//    @FindBy(id = "userLogin")
//    private WebElement login;
// 
//    /**
//     * ������
//     */
//    @FindBy(id = "regPassword")
//    private WebElement password;
// 
//    /**
//     * ������ �������������
//     */
//    @FindBy(id = "passwordConfirmation")
//    private WebElement passwordConfirm;
// 
//    /**
//     * E-mail
//     */
//    @FindBy(id = "UserEmail")
//    private WebElement email;
// 
//    /**
//     * ������ ����������������
//     */
//    @FindBy(id = "submitRegistration")
//    private WebElement bSubmitRegister;
// 
//    /**
//     * ��������� �� ������
//     */
//    @FindBy(id = "error-message")
//    private WebElement registerError;
// 
//    public RegistrationPage(WebDriver driver) {
//        // ���������, ��� �� ���������� �� ������ ��������
//        if (!driver.getCurrentUrl().contains(URL_MATCH)) {
//                    throw new IllegalStateException(
//                        "This is not the page you are expected"
//                        );
//        }
// 
//        PageFactory.initElements(driver, this);
//        this.driver = driver;
//    }
// 
//    /**
//     * ���������� ������������
//     * @param user - {@link User}
//     */
//    private void registerUser(User user) {
//        System.out.println(driver.getTitle());
//        login.sendKeys(user.login);
//        password.sendKeys(user.password);
//        passwordConfirm.sendKeys(user.passwordConfirmation);
//        email.sendKeys(user.email);
// 
//        bSubmitRegister.click();
//    }
// 
//    /**
//     * �������� ����������� ������������
//     * @param user - {@link User}
//     * @return {@link HomePage}
//     */
//    public HomePage registerUserSuccess(User user) {
//        registerUser(user);
//        return new HomePage(driver);
//    }
// 
//    /**
//     * ���������� �����������
//     * @param user - {@link User}
//     * @return {@link RegistrationPage}
//     */
//    public RegistrationPage registerUserError(User user) {
//        registerUser(user);
//        return new RegistrationPage(driver);
//    }
// 
//    /**
//     * ��������� ��������� �� ������
//     * @param user - {@link User}
//     * @return {@link RegistrationPage}
//     */
//    public RegistrationPage checkErrorMessage(String errorMessage) {
//        Assert.assertTrue("Error message should be present", 
//                        registerError.isDisplayed());
//        Assert.assertTrue("Error message should contains " + errorMessage, 
//                        registerError.getText().contains(errorMessage));
//        return this;
//    }
//}