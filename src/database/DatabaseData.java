package database;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import libs.Util;

import org.apache.log4j.Logger;

public class DatabaseData {
	Database database;
	Logger log;
	Util util;

	public DatabaseData(Database database) {
		this.database = database;
		log = Logger.getLogger(DatabaseData.class);
		util = new Util();
	}
	
	/*
	 * This method is used to select some value(id) fron database
	 * using select query
	 */
	
	public String selectIdByName(String name) throws SQLException {
		String id;
		id = database.selectValue("SELECT id FROM table WHERE name='" + name + "'");
		return id;
	}
	
	
	/*
	 * This method is used to print table from database
	 */
	
	public void getTableFromDB() throws SQLException, IOException {
		  List<ArrayList<String>> table;
		  table = database.selectTable("query that returns table");
		  util.printTable(table);
		 }
}