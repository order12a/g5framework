package libs;

import static libs.ConfigData.ui;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;


import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.seleniumhq.jetty7.util.log.Log;

public class CommonUsedWebElements {
	WebDriver driver;
	static final Logger log = Logger.getLogger(CommonUsedWebElements.class.getName());

	public CommonUsedWebElements(WebDriver driver) throws IOException {
		this.driver = driver;
		log.info("Constructor is created!");
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	}

	/*
	 * Open URL in a browser and verify page title
	 */

	public boolean open(String url, String title)
			throws ClassNotFoundException, IOException, InstantiationException,
			IllegalAccessException {
		driver.manage().window().maximize();
		driver.get(url);
//		driver.switchTo().frame(0);// It depends from app, could be commented if
									// not works
		
		String pageTitle = driver.getTitle();
		if (pageTitle.equals(title)) {
			log.info("Page: " + url + " succesfully opened,\npage title: " + title);
			return true;
		} else {
			log.error("ERROR, Couldn't open page: " + url);
			return false;
		}
	}
	
	/*
	 * This mehod is used to wait for some time
	 */
	
	public void waitForSomeTime(long miliseconds) throws InterruptedException {
		Thread.sleep(miliseconds);
	}

	/*
	 * This method is used to quit from test
	 */
	
	public void quit() {
		driver.quit();
	}

	/*
	 * This method is used to input some text into some text field
	 */

	public void inputText(String text, String xpathFieldLocator) throws IOException, ClassNotFoundException, IllegalAccessException, InstantiationException {

		WebElement textField;
		textField = driver.findElement(ui(xpathFieldLocator));
		textField.clear();
		textField.sendKeys(text);
		log.info(text + " was successfuly entered int textfield");
	}

	/*
	 * This method is used to click some button
	 */

	public void clickButton(String xpathButtonLocator) throws IOException, ClassNotFoundException, IllegalAccessException, InstantiationException {
		WebElement button;
		button = driver.findElement(ui(xpathButtonLocator));
		button.click();
	}

	/*
	 * This method is used to select appropriate radiobutton
	 */

	public void selectRadioButtonFromBlock(String radioButtonName,
			String radioButtonBlockLocator) throws IOException, ClassNotFoundException, IllegalAccessException, InstantiationException {

		String xpath = radioButtonBlockLocator + "[contains(text(), '"
				+ radioButtonName + "')]";

		WebElement radioButton;
		radioButton = driver.findElement(ui(xpath));
		radioButton.click();
	}
	

	/*
	 * This method is used to select or deselect checkbox for input as
	 * parameters this method takes check box state (YES or NO) and xpath
	 */

	public void selectCheckBox(String checkboxState,
			String xpathLocatorOfCheckBox) throws IOException, ClassNotFoundException, IllegalAccessException, InstantiationException {
		WebElement checkbox;
		checkbox = driver.findElement(ui(xpathLocatorOfCheckBox));
		if (checkbox.isSelected() && checkboxState.equals("YES")) {
			System.out.println("Checkbox is selected");
		} else if (!checkbox.isSelected() && checkboxState.equals("NO")) {
			System.out.println("Checkbox is not selected");
		} else if (checkbox.isSelected() && checkboxState.equals("NO")) {
			System.out.println("Here we deselect checkbox");
			checkbox.click();
		} else if (!checkbox.isSelected() && checkboxState.equals("YES")) {
			System.out.println();
			checkbox.click();
		}
	}

	/*
	 * This method is used to select element from dropdown list by user's click
	 */

	public void selectFromListByClick(String listValue, String listLocator)
			throws IOException, ClassNotFoundException, InstantiationException,
			IllegalAccessException {
		WebElement list;
		list = driver.findElement(ui(listLocator));

		new Select(list).selectByVisibleText(listValue); // This method realized
															// click() method in
															// it's structure
	}

	/*
	 * This method is used to select from drop-down list by first focus on list
	 */

//	public void selectFromSelectListByFocus(String listValue, String listLocator) throws IOException, ClassNotFoundException, IllegalAccessException, InstantiationException, InterruptedException {
//		WebElement list;
//		list = driver.findElement(ui(listLocator));
//		new Actions(driver).moveToElement(list).perform();
//		focusOnElement(listLocator);
//		waitForSomeTime(1000);
//		new Select(list).selectByVisibleText(listValue);
//	}
	
	public void selectMenuElementByFocus(String listValue, String listLocator) throws IOException, ClassNotFoundException, IllegalAccessException, InstantiationException, InterruptedException {
		
//		new Actions(driver).moveToElement(list).perform();
		focusOnElement(listLocator);
		waitForSomeTime(1000);
		driver.findElement(By.partialLinkText(listValue)).click();
	}

	/*
	 * This method is used to enter some text in field and click ENTER
	 */

	public void inputTextAndClickEnter(String text, String elementLocator) throws IOException, ClassNotFoundException, IllegalAccessException, InstantiationException {
		WebElement inputField;
		inputField = driver.findElement(ui(elementLocator));
		inputField.clear();
		inputField.sendKeys(text);
		inputField.sendKeys(Keys.ENTER);
	}
	
	/*
	 * This method is used to verify that expected text is present at
	 * the specific webelement
	 */
	
	public boolean verifyTextPresent(String elementLocator, String textToVerify) throws ClassNotFoundException, IllegalAccessException, InstantiationException, IOException {
		log.info("Trying to verify text at " + elementLocator + " with text to verify: " + textToVerify);
		WebElement element = driver.findElement(ui(elementLocator));
		String textFromElement = element.getText();
		return textToVerify.equals(textFromElement);
	}
	
	
	/*
	 * This method is used to check that target element is present at the page
	 */
	
	public boolean verifyElementIsPresent(String elementLocator) throws ClassNotFoundException, IllegalAccessException, InstantiationException, IOException {
		boolean result = true;
		WebElement element = driver.findElement(ui(elementLocator));
		if(element.isDisplayed()){
			log.info("Element - " + element.getText() + " is present at the page");
			result = true;
		}
		return result;
	}
	
	/*
	 * This method is used to verify that list of expected elements is present and displayed
	 * at the current page
	 */
	
	public boolean verifyElementsArePresent(List listOfElementsLocators) throws ClassNotFoundException, IllegalAccessException, InstantiationException, IOException {
		boolean result = true;
		for (Object object : listOfElementsLocators) {
			WebElement element = driver.findElement(ui((object.toString())));
			if(element.isDisplayed()){
				log.info("Element - " + element.getText() + " is present at the page");
				result = true;
			}
		}
		return result;	
	}

	
	/*
	 * This method is used to find link by text and click
	 */

	public void clickLinkByText(String linkText) throws IOException, ClassNotFoundException, IllegalAccessException, InstantiationException {
		WebElement link;
		link = driver.findElement(ui(linkText));
		link.click();
	}

	
	/*
	 * A Special option - selecting 2-nd submenu in drop-down list
	 */

	public void selectFromSecondSubMenuFromDropdownList(String listLocator,
			String mainListLocator, String textSubList) throws IOException, ClassNotFoundException, IllegalAccessException, InstantiationException {
		WebElement list;
		list = driver.findElement(ui(listLocator));
		// list.click();
		WebElement mainList = driver.findElement(ui(mainListLocator));
		// webElements.focusOnElement(mainListLocator);
		// new Actions(driver).click(list).moveToElement(mainList).perform();
		// new Select(mainList).selectByVisibleText(textSubList);

	}
	
	/*
	 * This method is use to select item from second submenu in
	 * the dropdown list by clicking
	 */

	public void selectFromSecondSubMenuFromDropdownListByClick(
			String dropDownLocator, String mainMenuLocator,
			String subMenuLocator) throws IOException, ClassNotFoundException, IllegalAccessException, InstantiationException {
		
		WebElement dropDownList;
		WebElement mainMenuItem;
		WebElement subMenuItem;
		
		dropDownList = driver.findElement(ui(dropDownLocator));
		mainMenuItem = driver.findElement(ui(mainMenuLocator));
		subMenuItem = driver.findElement(ui(subMenuLocator));

		dropDownList.click();
		mainMenuItem.click();
		subMenuItem.click();
	}

	/*
	 * This method is used to click at dropdown list item
	 * select element by it's name that we are looking for ->
	 * and select next item by visible text(in new menu)
	 */

	public void selectSubmenuFromMainMenuByAllXpathes(String dropDownLocator,
			String mainMenuLocator, String mainMenuItem, String subMenuLocator,
			String subMenuItem) throws IOException, ClassNotFoundException, IllegalAccessException, InstantiationException {
		WebElement dropDown;
		WebElement mainMenu;
		WebElement subMenu;
		
		dropDown = driver.findElement(ui(dropDownLocator));
		mainMenu = driver.findElement(ui(mainMenuLocator));
		subMenu = driver.findElement(ui(subMenuLocator));

		dropDown.click();
		new Select(mainMenu).selectByVisibleText(mainMenuItem);
		new Select(mainMenu).selectByVisibleText(subMenuItem);

	}


	/*
	 * This method is used to click at dropdown list -> navigate to
	 * next menu in this dropdown list and from this menu select
	 * and click on appropriate element
	 */

	public void clickFocusClick(String dropDownListLocator,
			String mainMenuLocator, String mainMenuText, String subMenuLocator,
			String subMenuItem, String subMenuText) throws IOException, ClassNotFoundException, IllegalAccessException, InstantiationException {
		WebElement dropDown;
		WebElement mainMenu;
		WebElement mainMenuItem;
		WebElement subMenu;
		String mainMenuItemXpath;

		dropDown = driver.findElement(ui(dropDownListLocator));
		mainMenu = driver.findElement(ui(mainMenuLocator));
		subMenu = driver.findElement(ui(subMenuLocator));

		mainMenuItemXpath = mainMenuLocator + "[contains(text(), '"
				+ mainMenuText + "')]";
		mainMenuItem = driver.findElement(By.xpath(mainMenuItemXpath));
		
		dropDown.click();
		new Actions(driver).moveToElement(mainMenuItem).perform();
		new Select(subMenu).selectByVisibleText(subMenuText);
	}

	/*
	 * This method is used to focus on specific element
	 */

	public void focusOnElement(String elementLocator) throws ClassNotFoundException, IllegalAccessException, InstantiationException, IOException, InterruptedException {
		
		WebElement element = driver.findElement(ui(elementLocator));
		new Actions(driver).moveToElement(element).perform();
	}

	/*
	 * Method is used to check that element is present on page.
	 */
	
	public boolean isElementPresent(By elementLocator) {
		try {
			driver.findElement(elementLocator);
		} catch (org.openqa.selenium.NoSuchElementException Ex) {
			return false;
		}
		return true;
	}
	
	/*
	 * This method is used to check that expected text is present 
	 * at target element
	 */
	
	public void checkTextIsPresent(String elementLocator, String expectedText) throws ClassNotFoundException, IllegalAccessException, InstantiationException, IOException {
		WebElement element = driver.findElement(ui(elementLocator));
		
	}
	
	/*
	 * This method is used to click on some element and navigate to opened new tab
	 */
	
	public void clickOnSomethingAndSwitchToNewTab(String locator) throws ClassNotFoundException, IllegalAccessException, InstantiationException, IOException {
		 Set<String> oldWindowsSet = driver.getWindowHandles();
		   driver.findElement(ui(locator)).click();
		   Set<String> newWindowsSet = driver.getWindowHandles();
		   newWindowsSet.removeAll(oldWindowsSet);
		   String newWindowHandle = newWindowsSet.iterator().next();
		   String windowHandler = driver.getWindowHandle();
		   driver.switchTo().window(newWindowHandle);
	}
	
	
	/*
	 * This method is used to check is the user logged in 
	 */
	
	public boolean isloggedIn(String envelopeIconLocator, String userProfileImageLocator) throws ClassNotFoundException, IllegalAccessException, InstantiationException, IOException, InterruptedException {
		log.info("Started checking condition if user is logged in");
		
		WebElement envelopIcon = driver.findElement(ui(envelopeIconLocator));
		WebElement userProfileImage = driver.findElement(ui(userProfileImageLocator));

		
		log.info("Envelope icon - " + envelopIcon.isDisplayed());
		log.info("User profile icon - " + userProfileImage.isDisplayed());
		waitForSomeTime(2000);
		if (userProfileImage.isDisplayed()) {
			log.info("User is Logged In");
			return true;
		} else {
			log.info("User isn't Logged In");
			return false;
		}
	}
	
	/*
	 * This method is used to make screenshot
	 * Format of fileLocationAndFormat should be next "c:\\tmp\\screenshot.png"
	 * 
	 * I used here method which takes class Name(page accorting PageObject pattern) 
	 * and add time when this screenshot is taken to fileName
	 */
	
	public void makeScreenShot(String fileLocation, String Format) {
		  File scrFile = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
		  String className = this.getClass().getName();
		  Date timeNow = new Date();
		  SimpleDateFormat format = new SimpleDateFormat("EEE MMM dd HH-mm-ss");
		  
		  // Now you can do whatever you need to do with it, for example copy somewhere
		  try {
		   String fileLocationNameAndFormat =  fileLocation + className + "_" + format.format(timeNow) + "." + Format;
		FileUtils.copyFile(scrFile, new File(fileLocationNameAndFormat), true);
		  } catch (IOException e) {
		   e.printStackTrace();
		  }
		 }
	
	
	/*
	 * This method is used to wait for getting response from all Ajax requests
	 */
	
	 public boolean waitForAjaxResponse(int timeoutSeconds) throws InterruptedException {
	  if (driver instanceof JavascriptExecutor) {
	   JavascriptExecutor jsDriver = (JavascriptExecutor) driver;

	   for (int i = 0; i < timeoutSeconds; i++) {
	    Long numberOfConnections = (Long) jsDriver.executeScript("return jQuery.active");
	    if (numberOfConnections instanceof Long) {
	     log.debug("Number of active jQuery Ajax calls is <" + numberOfConnections + ">");

	     if (numberOfConnections == 0)
	      break;
	    }
	    // stay(1);
	   }
	   return false;
	  } else {
	   log.info("Web Driver: <" + driver + "> cann't execute JavaScript");
	   return false;
	  }
	 }
	 
	 /*
	  * This methos is use to upload image using input tag
	  */
	 
	 public void uploadImage(String inputLocator, String pathToImage) throws ClassNotFoundException, IllegalAccessException, InstantiationException, IOException {
		  WebElement inputField;
		  inputField = driver.findElement(ui(inputLocator));
		  
		  inputField.sendKeys(pathToImage);
		 }
	 
	 
	 /*
	  * This method is used to log out user 
	  * works at every page
	  */
	 
	 public void logOut(String menuItemLocator, String menuItem) throws IOException, ClassNotFoundException, IllegalAccessException, InstantiationException, InterruptedException{
		 	waitForSomeTime(2000);
			focusOnElement(menuItemLocator);
			waitForAjaxResponse(1000);
			driver.findElement(By.partialLinkText(menuItem)).click();
	 }
}
